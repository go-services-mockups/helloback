# Guia de Uso

## Sabores o Niveles de Limites actuales

| SABOR | Reservado |   | Limite |  | 
| --- | --- | ---   | --- | --- | 
| small | 0.1 | 0,256 | 1 | 2 | 
| medium | 0.5 | 1 | 1 | 4 | 
| large | 0.5 | 1.5 | 1.5 | 8   |
|           | CPU | RAM | CPU | RAM |

## Aplicar Limites a Nivel Namespace

Cuando aplicamos limites a nivel proyecto las mismas son objetos _quota_ en OpenShift.

Para aplicar las quotas de core y memoria para todo el proyecto deberemos ejecutar:

* Aplicar el limite elegido al proyecto especifico

En este caso usamos un limite SMALL en el proyecto myapp

~~~
$ oc create -f small/resourcequota-compute-resources.yaml -n myapp
~~~

**Nota: si no definimos el "-n myapp" estaremos aplicando el limite en el namespace actual**

* Para validar los valores de la quota aplicada:

~~~
$ oc get quota

NAME                   AGE     REQUEST                                          LIMIT
small-project-limits   3m53s   requests.cpu: 0/100m, requests.memory: 0/256Mi   limits.cpu: 0/1, limits.memory: 0/2Gi

$ oc describe quota small-project-limits 

Name:            small-project-limits
Namespace:       my-app
Resource         Used  Hard
--------         ----  ----
limits.cpu       0     1
limits.memory    0     2Gi
requests.cpu     0     100m
requests.memory  0     256Mi

$ 
~~~

En la consola web lo podemos ver en _Administraction_ -> _Resource Quotas_

![Resource_Quota](resource_quotas.png)

## Aplicar Limites a Nivel de Pods

Para poder aplicar limites a nivel cada objeto necesitaremos definir  limitranges en nuestro proyecto.

**Nota: Al crear los limitranges tambien  podremos definir limities y request por default para todos los objetos que no los tengan definidos al crearse**

### Limits y requests a nivel POD

![Limits_and_request](https://blog.kubecost.com/assets/images/k8s-recs-ands-limits.png)

## Aplicar limites y requests a nivel pods

Cuando aplicamos limites a nivel pods u otro objeto los mismos son objetos _limitrange_ en OpenShift.

Para aplicar los limites de core y memoria deberemos ejecutar:

* Aplicar el limite elegido al proyecto especifico

En este caso usamos un limite SMALL en el proyecto myapp

~~~
$ oc create -f small/limitrange-resource-limits.yaml -n myapp
~~~

**Nota: si no definimos el "-n myapp" estaremos aplicando el limite en el namespace actual**

* Para validar los valores del limite aplicado:

~~~
$ oc describe limitrange

Name:       small-limits-por-pod
Namespace:  my-app
Type        Resource  Min  Max  Default Request  Default Limit  Max Limit/Request Ratio
----        --------  ---  ---  ---------------  -------------  -----------------------
Container   cpu       -    1    100m             200m           10
Container   memory    -    2Gi  100Mi            512Mi          -
Pod         cpu       -    1    -                -              10
Pod         memory    -    2Gi  -                -              -
$ oc get limitrange
NAME                   CREATED AT
small-limits-por-pod   2021-07-27T15:32:00Z

$ oc describe limitrange

Name:       small-limits-por-pod
Namespace:  my-app
Type        Resource  Min  Max  Default Request  Default Limit  Max Limit/Request Ratio
----        --------  ---  ---  ---------------  -------------  -----------------------
Container   memory    -    2Gi  100Mi            512Mi          -
Container   cpu       -    1    100m             200m           10
Pod         cpu       -    1    -                -              10
Pod         memory    -    2Gi  -                -              -

$
~~~

En la consola web lo podemos ver en _Administraction_ -> _Limits Ranges_

![Limit Ranges](limit_ranges.png)

