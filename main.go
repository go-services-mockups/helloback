package main

import (
	"fmt"
	"os"
	"log"
	"net/http"
	"strconv"
	"time"
        "math/rand"
)

var req_budget int = 10
var startup int = 5
var working int = 3
var refill int = 30
var err_counter int = 0
var ok_counter = 0
var reset_time_global = 60

func Handler(w http.ResponseWriter, r *http.Request) {
	if req_budget >= 0 {
		req_budget = req_budget - 1
		w.WriteHeader(http.StatusOK)
		fmt.Fprintln(w, "Quedan "+strconv.Itoa(req_budget)+" Request")
		ok_counter = ok_counter + 1
		//w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusServiceUnavailable)
		fmt.Fprintln(w, "No Quedan Request")
		err_counter = err_counter + 1
	}
	rand.Seed(time.Now().UnixNano())
	    // TODO, check that working is >= 0 , in order to avoid Intn to fail
        n := rand.Intn(working+1) // n will be between 1 and 11
        fmt.Printf("Sleeping %d seconds...\n", n)
        time.Sleep(time.Duration(n)*time.Second)
}

func RefillHandler(w http.ResponseWriter, r *http.Request) {
	req_budget = req_budget + refill
	fmt.Fprintln(w, "Added "+strconv.Itoa(refill)+" Requests")
	ok_counter = ok_counter + 1
}

func HealthzHandler(w http.ResponseWriter, r *http.Request) {
        if req_budget >= 0 {
		w.WriteHeader(http.StatusOK)
		fmt.Fprintln(w, "OK - Still have budget left")
	}else{
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintln(w, "KO - No budget Left, please refill")
	}
}

func MetricsHandler(w http.ResponseWriter, r *http.Request) {
        // Not a really good healthz check :S
        fmt.Fprintln(w, "ok_counter " + strconv.Itoa(ok_counter) + "\nerr_counter " + strconv.Itoa(err_counter) )
}

func main() {

	// Leemos del entorno las variables
	startup, _ = strconv.Atoi(os.Getenv("STARTUP"))
	req_budget, _ = strconv.Atoi(os.Getenv("BUDGET"))
	working, _ = strconv.Atoi(os.Getenv("LATENCY"))

	fmt.Println("El tiempo de startup es " + strconv.Itoa(startup))
        fmt.Println("El budget inicial es " + strconv.Itoa(req_budget))
        fmt.Println("La latencia maxima para procesar requests es " + strconv.Itoa(working))


	time.Sleep(time.Duration(startup) * time.Second)
	http.HandleFunc("/", Handler)
	http.HandleFunc("/refill", RefillHandler)
        http.HandleFunc("/healthz", HealthzHandler)
        http.HandleFunc("/metrics", MetricsHandler)
        go func(reset_time int) {
                       for {
                 fmt.Println("Reseting Metrics in " + strconv.Itoa(reset_time) + " Seconds")
                 time.Sleep(time.Duration(reset_time) * time.Second)
                 ok_counter = 0
                 err_counter = 0
                  }
        }(reset_time_global)
	log.Fatal(http.ListenAndServe(":8888", nil))
}
